import React, { Component } from 'react';

import './App.css';
import './bootstrap.min.css';



class App extends Component{
  constructor(){
    super();
    this.state = {
      sayfa : "Kullanici",  // Admin , Raporlar, Kullanici
    }
  }  
  changePage(to){
    this.setState({sayfa:to});
  }
  render(){
    return <div>  

    {this.state.sayfa==="" ? <a href="#" onClick={this.changePage.bind(this,"Kullanici")}> Kullanici Sayfasi</a>:null}
    {this.state.sayfa==="" ? <br/>:null}
    {this.state.sayfa==="" ? <br/>:null}
    {this.state.sayfa==="" ? <a href="#" onClick={this.changePage.bind(this,"Admin")}> Admin Sayfasi</a> :null}
    {this.state.sayfa==="" ? <br/>:null}
    {this.state.sayfa==="" ? <br/>:null}


    {this.state.sayfa==="Raporlar" ? <a href="#" onClick={this.changePage.bind(this,"Admin")}> Admin Sayfasi</a> :null}
    {this.state.sayfa==="Admin" ? <a href="#" onClick={this.changePage.bind(this,"Raporlar")}> Raporlar Sayfasi</a>:null}    
    

    {this.state.sayfa==="Admin" ? <RotaOlustur/>:null}
    {this.state.sayfa==="Admin" ? <Gezinoktasiolusturucu/>:null}
    {this.state.sayfa==="Admin" ? <RotaOnayla/>:null}
    {this.state.sayfa==="Raporlar" ? <TanimalananSeyahatler/>:null}        
    {this.state.sayfa==="Raporlar" ? <Mevcutrotalar/>:null}
    {this.state.sayfa==="Raporlar" ? <Gezinoktalari/>:null}    
    {this.state.sayfa==="Kullanici" ? <Seyahatolustur/>:null}
    {this.state.sayfa==="Kullanici" ? <RotaOner/>:null}    


    

    </div>
  }
}

class RotaOnayla extends Component{
  constructor(){
    super();  
    this.state = {
      items : [{"_id":1,"ankara":"2yild","aydin":"super otel"}]
    }
    fetch(" http://0.0.0.0/rotaOnerileri").then(res => res.json())
    .then(
      (result) => {
        this.setState({
          items: result
        });
      }
    )
  }

  render() {
    var rotalar = [];
    for (var i = 0; i<this.state.items.length ; i++){
      var oge = this.state.items[i];
      var keyler = Object.keys(oge);
      var rota = [];
      for (var j = 0; j<keyler.length; j++){
        if (keyler[j] !== "_id"){
          rota.push([keyler[j],oge[keyler[j]]])
        }
      }
      rotalar.push(rota);
    }


    return <div className="App">
      <br/><hr/> Önerilen Rotalar <br/>
      <hr/><br/>
      {rotalar.map((rota)=>{
        var ret = rota.map((yer)=>{
          return <tr> <td>{yer[0]}</td> <td> {yer[1]}</td> </tr>
        })
        ret = ret.concat([<div><br/></div>])
        return React.createElement("div",{},[React.createElement("table", {}, ret)]);
      })}
      <br/><br/><hr/>
    </div>  
  }
}

class RotaOner extends Component{
  constructor(){
    super();
    this.state = {
      yerler : [
        {
          "KAYSERI": "3YILDIZ",
          "_id": {
            "$oid": "5ae4d13160c558000c84df3b"
          }
        }
      ],
      secilenler : {
        0:{"":""},
        2:{"":""},
        1:{"":""},
        3:{"":""},
        4:{"":""},
        //1:{"Kayseri":"3YILDIZ"}
      }
    }
    fetch("http://0.0.0.0/yerler").then(res => res.json())
    .then(
      (result) => {
        this.setState({
          yerler: result
        });
      },
    )     
  }

  
  handleChange(no,event){
    var k = this.state.secilenler;

    var obj = JSON.parse(event.target.value);
    k[no] = [obj[0],obj[1]];
  
    this.setState({secilenler:k});

  }

  onayla(){
    var base = "http://0.0.0.0/rotaOnerisi/ekle?";
    for(var i = 0; i<5; i++){
      if (this.state.secilenler[i][0] !== undefined){
      var key = this.state.secilenler[i][0];
      var val = this.state.secilenler[i][1];
      base += key + "="+val+"&";
      }

    }
    console.log(base);
    fetch(base);
  }  
  
  render(){
    var yerler = [""]  
    for (var i = 0; i<this.state.yerler.length ; i++){
      var oge = this.state.yerler[i];
      var keyler = Object.keys(oge);
      for (var j = 0; j<keyler.length; j++){
        if (keyler[j] !== "_id"){
          yerler.push([keyler[j],oge[keyler[j]]])
        }
      }
    }
    
    return <div className="App">
      <br/><hr/> Rota Öner <br/>
      <hr/><br/>
      
      {[0,1,2,3,4].map((sira)=>{
        var sel = yerler.map((yer)=>{
          return <option value={'["'+yer[0]+'","'+yer[1]+'"]'}> {yer[0]} => {yer[1]} </option>
        })
        var k = <div><select onChange={this.handleChange.bind(this,sira)}> {sel} </select> <br/> </div>
        return k
      })}  
      <form action="." method="get">
      <button onClick={this.onayla.bind(this)}> Rota Öner</button>
      </form>
      <br/><br/><hr/>
    </div>  

  }
  
}

class RotaOlustur extends Component{
  constructor(){
    super();
    this.state = {
      yerler : [
        {
          "KAYSERI": "3YILDIZ",
          "_id": {
            "$oid": "5ae4d13160c558000c84df3b"
          }
        }
      ],
      secilenler : {
        0:{"":""},
        2:{"":""},
        1:{"":""},
        3:{"":""},
        4:{"":""},
        //1:{"Kayseri":"3YILDIZ"}
      }
    }
    fetch("http://0.0.0.0/yerler").then(res => res.json())
    .then(
      (result) => {
        this.setState({
          yerler: result
        });
      },
    )     
  }

  
  handleChange(no,event){
    var k = this.state.secilenler;

    var obj = JSON.parse(event.target.value);
    k[no] = [obj[0],obj[1]];
 
    this.setState({secilenler:k});

  }

  onayla(){
    var base = "http://0.0.0.0/rota/ekle?";
    for(var i = 0; i<5; i++){
      if (this.state.secilenler[i][0] !== undefined){
      var key = this.state.secilenler[i][0];
      var val = this.state.secilenler[i][1];
      base += key + "="+val+"&";
      }

    }
    console.log(base);
    fetch(base);
  }  
  
  render(){
    var yerler = [""]  
    for (var i = 0; i<this.state.yerler.length ; i++){
      var oge = this.state.yerler[i];
      var keyler = Object.keys(oge);
      for (var j = 0; j<keyler.length; j++){
        if (keyler[j] !== "_id"){
          yerler.push([keyler[j],oge[keyler[j]]])
        }
      }
    }
    
    return <div className="App">
      <br/><hr/> Rota Olustur <br/>
      <hr/><br/>
      
      {[0,1,2,3,4].map((sira)=>{
        var sel = yerler.map((yer)=>{
          return <option value={'["'+yer[0]+'","'+yer[1]+'"]'}> {yer[0]} => {yer[1]} </option>
        })
        var k = <div><select onChange={this.handleChange.bind(this,sira)}> {sel} </select> <br/> </div>
        return k
      })}  
      <form action="." method="get">
      <button onClick={this.onayla.bind(this)}> Rota Oluştur</button>
      </form>
      <br/><br/><hr/>
    </div>  

  }
}

class Mevcutrotalar extends Component{
  constructor(){
    super();  
    this.state = {
      items : [{"_id":1,"ankara":"2yild","aydin":"super otel"}]
    }
    fetch("http://0.0.0.0/rotalar").then(res => res.json())
    .then(
      (result) => {
        this.setState({
          items: result
        });
      }
    )
  }

  render() {
    var rotalar = [];
    for (var i = 0; i<this.state.items.length ; i++){
      var oge = this.state.items[i];
      var keyler = Object.keys(oge);
      var rota = [];
      for (var j = 0; j<keyler.length; j++){
        if (keyler[j] !== "_id"){
          rota.push([keyler[j],oge[keyler[j]]])
        }
      }
      rotalar.push(rota);
    }


    return <div className="App">
      <br/><hr/> Mevcut Rotalar <br/>
      <hr/><br/>
      {rotalar.map((rota)=>{
        var ret = rota.map((yer)=>{
          return <tr> <td>{yer[0]}</td> <td> {yer[1]}</td> </tr>
        })
        ret = ret.concat([<div><br/></div>])
        return React.createElement("div",{},[React.createElement("table", {}, ret)]);
      })}
      <br/><br/><hr/>
    </div>  
  }
}
class Gezinoktalari extends Component{
  constructor(){
    super();
    this.state = {
      items : [
        {
          "KAYSERI": "3YILDIZ",
          "_id": {
            "$oid": "5ae4d13160c558000c84df3b"
          }
        }
      ]
    }
    fetch("http://0.0.0.0/yerler").then(res => res.json())
    .then(
      (result) => {
        this.setState({
          items: result
        });
      },
    )     
  }

  render(){
    var yerler = []  
    for (var i = 0; i<this.state.items.length ; i++){
      var oge = this.state.items[i];
      var keyler = Object.keys(oge);
      for (var j = 0; j<keyler.length; j++){
        if (keyler[j] !== "_id"){
          yerler.push([keyler[j],oge[keyler[j]]])
        }
      }
    }

    return <div className="App">
      <br/><hr/> Gezi Noktaları <br/>
      <hr/><br/>
      <table>
      <tr>
        <th>Yer</th>  
        <th>Otel</th>  
      </tr>  
      {yerler.map((yer)=>{
        return <tr> <td>{yer[0]}</td><td>{yer[1]}</td> </tr>
      })}
      </table>
      <br/><br/><hr/>
    </div>  
  }
}

class TanimalananSeyahatler extends Component{
  // TODO: id den rota çekme
  constructor(){
    super();
    this.state = {
      items : [{
        "rota_id": "5ae4bd9660c558000ca447e3",
        "sigorta": "False",
        "_id": {
          "$oid": "5ae4cb4960c558000c84df38"
        },
        "arac": "Uçak",
        "musteri_adi": "kadir",
        "isRotaLoad": false,
        'rota':[]
      }]
    }
    fetch("http://0.0.0.0/seyahatler").then(res => res.json())
    .then(
      (result) => {
        var tmp = result;
        result.isRotaLoad = false;
        result.rota = [];  
        this.setState({
          items: tmp,
        });
      },

      (error) => {}
    )    
  }    

  getRota(i){
    var id = this.state.items[i].rota_id;
    fetch("http://0.0.0.0/rota?id="+id).then(res => res.json())
    .then(
      (result)  => {
        var rotalar = [];
        var oge = result;
        var keyler = Object.keys(oge);
        for (var j = 0; j<keyler.length; j++){
          if (keyler[j] !== "_id"){
            rotalar.push([keyler[j],oge[keyler[j]]])
          }
        }
        
        var tmp = this.state;
        tmp.items[i].isRotaLoad = true;
        tmp.items[i].rota = rotalar;
        this.setState(tmp);

      }
    )
  }  
  render(){
    return <div className="App">
      <br/><hr/> Tanımlanan Seyahatler <br/>
      <hr/><br/>
      <table className="genis">
      <tr>
      <th>Müşteri Ad Soyad</th>  
      <th>Güzergah</th>  
      <th>Sigorta</th>  
      <th>Ulaşım</th>  
      </tr>
      {this.state.items.map((item,i)=>{ 
        return <tr>  <td>{item.musteri_adi}</td>
                        <td onClick={this.getRota.bind(this,i)}>
                            {item.isRotaLoad ? item.rota.map(rot=>{ return <div>{rot[0]} - {rot[1]}</div>}) : <a href="#">{item.rota_id}</a> }
                          </td><td>{item.sigorta}</td><td>{item.arac}</td><br/> </tr> 
      })}
      </table>
      <br/><br/><hr/>
    </div>  
  }  
}  

class Gezinoktasiolusturucu extends Component{
  constructor(){
    super();
    this.state = {
      yer : "",
      otel : ""
    }
  }  

  olustur(){
    var k = 'http://0.0.0.0/yer/ekle?yer='+this.state.yer+'&otel='+ this.state.otel;
    fetch(k);
  } 

  handleYer(event){
    this.setState({yer: event.target.value});
  }

  handleOtel(event){
    this.setState({otel: event.target.value});
  }  

  render(){
    return <div className="App">
      <br/><hr/> Gezi noktasi oluştur <br/>
      <hr/><br/>
      <form action="." method="get">      
        Yer: <br/>  
        <input onChange={this.handleYer.bind(this)}></input>  <br/>  
        Otel: <br/>  
        <input onChange={this.handleOtel.bind(this)}></input>  <br/>  
        
        <button  onClick={this.olustur.bind(this)}> Gezi Noktasını Oluştur</button>
        </form>
      <br/><br/><hr/>
    </div>  
  }
}

class Seyahatolustur extends Component{
  constructor(){
    super();
    this.state = {
      musteri_adi : "",
      sigorta : "True",
      ulasim : "Otobüs",
      rota_id : null
    }
  } 

  handleSigortaChange(event) {
    this.setState({sigorta: event.target.value});
  }

  handleUlasimChange(event) {
    this.setState({ulasim: event.target.value});
  }  

  handleRotaIdChange(event) {
    this.setState({rota_id: event.target.value});
  }    

  handleMusteriChange(event) {
    this.setState({musteri_adi: event.target.value});
  }      

  yolculukTamamla(){
    if (this.state.rota_id == null){
      alert("Lütfen Bir Güzergah seçin");
    }
    else{
      fetch('http://0.0.0.0/seyahat/ekle?rota_id=' + this.state.rota_id + '&ulasimAraci=' + this.state.ulasim +'&sigorta='+this.state.sigorta+'&musteri='+this.state.musteri_adi)    }
  }  
  
  render(){
    return <div className="App">
      <br/><hr/> Seyahat Tanımlama <br/>
      <hr/>
      <br/> Güzergah <br/>

      <form action="." method="get">      
        <Rotalar handleChange = {this.handleRotaIdChange.bind(this)}/> <br/>
        Sigorta <br/>
        <Sigorta handleChange = {this.handleSigortaChange.bind(this)}/> <br/>
        Ulaşım <br/>
        <Ulasim handleChange = {this.handleUlasimChange.bind(this)}/> <br/>
        Ad Soyad <br/>
        <Musteri handleChange = {this.handleMusteriChange.bind(this)}/> <br/>        
        <br/>
        
        <button onClick={this.yolculukTamamla.bind(this)} type="subbmit">Yolculuk Kaydını Tamamla</button>
      </form>

      <br/><br/><br/>
      <hr/>      
    </div>  
  }
}

class Musteri extends Component{
  render(){
    return <input onChange={this.props.handleChange}>
    </input>  
  }
}  

class Ulasim extends Component{
  render(){
    return <select onChange={this.props.handleChange}>
      <option>Otobüs</option>    
      <option>Uçak</option>    
      </select>  
  }
}  

class Sigorta extends Component{
  render(){
    return <select onChange={this.props.handleChange}>
      <option value="True">True</option>    
      <option value = "False">False</option>    
    </select>  
  }
}

class Rota extends Component{
  render(){
    return  <option value= {this.props.id.$oid}>  
    
    {this.props.icerik.map((ic)=>{
      return <p>{ic[0]}:{ic[1]}  |  </p>
    })}
    
    </option>
  }
}

class Rotalar extends Component {
  constructor(){
    super();  
    this.state = {
      items : [1,2,3],
      items2 : [{"_id":1,"ankara":"2yild"}]
    }
    fetch("http://0.0.0.0/rotalar").then(res => res.json())
    .then(
      (result) => {
        this.setState({
          isLoaded: true,
          items2: result
        });
      },
      // Note: it's important to handle errors here
      // instead of a catch() block so that we don't swallow
      // exceptions from actual bugs in components.
      (error) => {
        this.setState({
          isLoaded: true,
        });
      }
    )
  }
  render() {
    var tmp;  
    return (
      <select onChange={this.props.handleChange}>
      <option></option>
      {this.state.items2.map((item)=>{
        var keyler = Object.keys(item);
        var rota = [];
        for (var j = 0; j<keyler.length; j++){
          if (keyler[j] !== "_id"){
            rota.push([keyler[j],item[keyler[j]]])
          }
        }
        return <Rota icerik={rota} id={item['_id']}></Rota>;
      })}
      </select>
    );
  }
}

export default App;

import random, os, json, datetime, time

from flask import Flask, Response, jsonify, request
from pymongo import MongoClient
from bson import json_util, ObjectId
import pprint
import json
from bson.json_util import dumps,loads


app = Flask(__name__)
client = MongoClient('mongo', 27017)
db = client.test_db


@app.route("/rotalar")
def ind():
    coll = db.rotalar
    k = []    
    for i in coll.find():
        k.append(i)
    k = dumps(k)
    return k

@app.route("/seyahat/ekle")
def addSeyahatler():
    coll = db.seyahatler
    rota_id = request.args.get("rota_id")
    arac = request.args.get("ulasimAraci")
    sigorta = request.args.get("sigorta")
    musteri_adi = request.args.get("musteri")
    seyahat = {"rota_id":rota_id, "arac":arac, "sigorta":sigorta, "musteri_adi": musteri_adi}
    coll.insert(seyahat)
    k = dumps(seyahat)
    return k

@app.route("/seyahatler")
def getSeyahatler():
    coll = db.seyahatler
    k = []    
    for i in coll.find():
        k.append(i)
    k = dumps(k)
    return k
        
@app.route("/rotaOnerisi/ekle")    
def addOneri():    
    coll = db.rotaOnerileri
    rota = {}
    for i,j in zip(request.args.keys(), request.args.values()):
        rota[i.upper()] = j.upper()
    coll.insert_one(rota)
    k = dumps(rota)
    return k    

@app.route("/rotaOnerileri")
def getOneri():
    coll = db.rotaOnerileri
    k = []    
    for i in coll.find():
        k.append(i)
    k = dumps(k)
    return k

@app.route("/rota/ekle")
def addRota():
    coll = db.rotalar
    rota = {}
    for i,j in zip(request.args.keys(), request.args.values()):
        rota[i.upper()] = j.upper()
    coll.insert_one(rota)
    k = dumps(rota)
    return k



@app.route("/rota")
def getRota():
    coll = db.rotalar
    k = coll.find_one(  {"_id": ObjectId(request.args.get("id")) } )
    k = dumps(k)
    return k


@app.route("/yerler")
def getYerler():
    coll = db.yerler    
    k = []    
    for i in coll.find():
        k.append(i)
    k = dumps(k)
    return k

@app.route("/yer/ekle")
def addYerler():
    coll = db.yerler    
    yer = {request.args.get("yer").upper():request.args.get("otel").upper()}
    coll.insert_one(yer)
    k = dumps(yer)
    return k

@app.route("/temizle")    
def temizle():
    colls = [db.yerler, db.rotalar, db.seyahatler]
    for i in colls:
        i.drop()        



if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.config['DEBUG'] = True
    app.run(host='0.0.0.0', port=port)